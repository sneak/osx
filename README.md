# osx

New deal for Catalina/Big Sur: Imaging is dead.  Install a fresh install
manually, then do the following.

# Manual Prerequisite Steps

Open System Preferences.

* Sharing > Set Hostname
* Security and Privacy > FileVault > Enable

Open a terminal and run the following:

* `chsh -s /bin/bash` and enter password
* `sudo xcodebuild -license accept` and click ok/install on developer tools
  installer popup window
* `sudo xcode-select --install`
# The Install

```bash
bash <(curl -s https://git.eeqj.de/sneak/osx/raw/branch/master/install.sh)
```

# Uninstall (not 100%, only for dev)
```bash
rm -rfv ~/.profile ~/.bashrc ~/Library/Homebrew ~/Library/bashrc.d ~/Library/profile.d ~/Library/user-setup $TMPDIR/osx
```