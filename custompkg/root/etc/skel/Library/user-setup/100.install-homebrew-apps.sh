#!/bin/bash

# reminder: homebrew is *spyware*. if you don't do this, it won't
# set HOMEBREW_NO_ANALYTICS and it will silently spy on you when you use it.
# this should happen in the .profile but let's do it again here to be certain:
source $HOME/.bashrc
export HOMEBREW_NO_ANALYTICS=1

# this shouldn't be needed but it is
export PATH="$PATH:$HOME/Library/Homebrew/bin"

# Homebrew's package database is on GitHub.  The hashes to verify the
# authenticity of the source archive downloads are in the package database.
# Microsoft, a large US military contractor with long ties to illegal NSA spying
# programs, bought GitHub and can exercise control over them.

# What this means is that a large US military contractor has control over the
# precise software that gets installed on your computer when you
# type "brew install x".  Cool, huh?

HOMEBREW_PKGS="
    asciinema
    autoconf
    automake
    bonnie++
    bwm-ng
    byobu
    cksfv
    cmake
    coreutils
    curl
    daemontools
    docker
    docker-compose
    docker-machine
    duplicity
    ffmpeg
    fish
    flac
    geoip
    gist
    git
    gnupg
    gnupg2
    go
    homebrew/dupes/rsync
    httpie
    httrack
    irssi
    jq
    kubernetes-cli
    lame
    mosh
    mtr
    mutt
    neovim
    nmap
    offlineimap
    p7zip
    par2
    pbzip2
    pgpdump
    pinentry-mac
    protobuf
    pv
    pwgen
    python3
    rename
    socat
    sox
    speedtest_cli
    tcptraceroute
    tor
    torsocks
    trash
    unrar
    unzip
    vimpager
    watch
    wget
    whatmask
"

for PKG in $HOMEBREW_PKGS ; do
    brew install --build-from-source $PKG
done
