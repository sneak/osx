#!/bin/bash

# set up paths and whatnot and ensure homebrew's spyware is disabled
source $HOME/.profile
export HOMEBREW_NO_ANALYTICS=1

# this shouldn't be needed but it is
export PATH="$PATH:$HOME/Library/Homebrew/bin"


# per default settings apps are installed in homedir
if [[ ! -d ~/Applications ]]; then
    mkdir -p ~/Applications
fi

# install caskroom
brew tap homebrew/cask

# the following is required for fetching some of the fonts
brew install --build-from-source svn

# no google-drive-file-stream because it requires admin privs and installs to /Applications systemwide
CASKS="
    alacritty
    atom
    bitwarden
    deluge
    disk-inventory-x
    eloston-chromium
    homebrew/cask/handbrake
    iterm2
    signal
    homebrew/cask/syncthing
    tor-browser
    vlc
    wasabi-wallet
    xscreensaver
"

for CASK in $CASKS ; do
    brew install --appdir="$HOME/Applications" $CASK
done

brew tap homebrew/cask-fonts

FONTS="
    font-consolas-for-powerline
    font-dejavu-sans-mono-for-powerline
    font-droid-sans-mono-for-powerline
    font-fira-mono-for-powerline
    font-inconsolata-dz-for-powerline
    font-inconsolata-for-powerline
    font-inconsolata-g-for-powerline
    font-liberation-mono-for-powerline
    font-menlo-for-powerline
    font-meslo-for-powerline
    font-monofur-for-powerline
    font-noto-mono-for-powerline
    font-roboto-mono-for-powerline
    font-source-code-pro-for-powerline
    font-ubuntu-mono-derivative-powerline
"

for FONT in $FONTS ; do
    brew install $FONT
done
