#!/bin/bash

if command -v gcloud >/dev/null 2>&1 >/dev/null ; then
  gcloud config set disable_usage_reporting true >/dev/null 2>&1
fi