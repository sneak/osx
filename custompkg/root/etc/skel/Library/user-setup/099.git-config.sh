#!/bin/bash

# a small vote for the status quo, or perhaps just a vote against the cost of change vs the zero benefit
git config --global init.defaultBranch master
