#!/bin/bash

# set up paths and whatnot
source $HOME/.profile

YARN_PKGS="
    now
    prettier
"

for PKG in $YARN_PKGS ; do
    yarn global add $PKG
done
