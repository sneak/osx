#!/bin/bash
############################################# install homebrew under homedir

# reminder: homebrew is *spyware*. using it in the default configuration is
# *unsafe*. other scripts elsewhere set HOMEBREW_NO_ANALYTICS to disable 
# its spying; do not use this file in isolation.

if [[ ! -d ~/Library/Homebrew ]]; then
    mkdir -p ~/Library/Homebrew
    cd ~/Library/Homebrew
    curl -fsSL https://github.com/Homebrew/brew/tarball/master | \
        tar --strip-components 1 -xvf -
fi

~/Library/Homebrew/bin/brew analytics off