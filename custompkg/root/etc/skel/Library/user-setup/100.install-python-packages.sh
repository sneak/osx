PYTHON_PKGS="
    awscli
    awsebcli
    pipenv
    powerline-shell
    virtualenv
"


# first, upgrade pip:

/usr/bin/pip3 install --upgrade --user pip

for PKG in $PYTHON_PKGS ; do
    /usr/bin/pip3 install --user $PKG
done
