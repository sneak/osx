#!/bin/bash

function run_install {

    mkdir -p $HOME/Library/Caches/Homebrew
    rm -rf $HOME/Library/Caches/Homebrew/*

    REPO="https://git.eeqj.de/sneak/osx.git"
    DEST="$TMPDIR/osx"

    if [[ ! -d "$DEST" ]]; then
        git clone "$REPO" "$DEST"
    else
        cd "$DEST" && git pull && cd -
    fi

    rsync -avP "$DEST/custompkg/root/etc/skel/" "$HOME/"
    mkdir -p $HOME/Library/bashrc.d
    mkdir -p $HOME/Library/profile.d
    touch $HOME/Library/bashrc.d/000keep.sh
    touch $HOME/Library/profile.d/000keep.sh
    
    # FIXME move this into the modular setup scripts
    mkdir -p "$HOME/Library/Desktop Pictures"
    rsync -avP $TMPDIR/osx/custompkg/root/Library/Desktop?Pictures/ $HOME/Library/Desktop?Pictures/
    defaults write \
        ~/Library/Preferences/com.apple.systempreferences DSKDesktopPrefPane \
        '<dict><key>UserFolderPaths</key><array><string>/Users/user/Library/Desktop Pictures</string></array></dict>'
    
    # run modular setup scripts
    for FN in "$HOME/Library/user-setup/"*.sh ; do
        echo "new-user-setup: starting $(basename "$FN")..."
        # we used to run these in a subshell but not we don't because we want the environment to persist between
        source "$FN"
        rm "$FN"
        echo "new-user-setup: removed $(basename $FN)..."
    done
}

run_install
